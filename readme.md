# About this FILMS APP 
This project is a mobile application developed with the ionic framework that consumes the [themoviedb](https://www.themoviedb.org) services.
-

Features
- Home - View showing new, current and popular films.
- Search - View that helps you search for films by name.
- Favorite - View that shows favourite films selected by the user.
- Detail - View showing the detail of a film with the option to save it as a favourite.

Technologies
- [Themoviedb](https://www.themoviedb.org) - Service used as backend.
- [Storage](https://ionicframework.com/docs/angular/storage) - Data persistence used to store favourite movies.
- Slides - View to show movies.
- Pipes - Data transformation.

## Available Scripts

In the project directory, you can run:

### `npm i`

Install the dependencies that the project needs.

### `npm run start`

Runs the app in the development mode.\
Open [http://localhost:8100](http://localhost:8100) to view it in the browser.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.