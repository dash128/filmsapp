import { Component, Input, OnInit } from '@angular/core';
import { MoviesService } from '../../services/movies.service';
import { IPelicula, IPeliculaDetalle, IRespuestaCredito, IActor } from '../../interfaces/interfaces';
import { ModalController } from '@ionic/angular';
import { DataLocalService } from '../../services/data-local.service';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.scss'],
})
export class DetalleComponent implements OnInit {
  @Input() id: string;
  pelicula: IPeliculaDetalle = {};
  estrella = 'star-outline';
  actores: IActor[] = [];
  oculto = 150;
  slideOptActores = {
    slidesPerView:3.3,
    freeMode: true,
    spacebetween: -5
  };

  constructor(
    private moviesService: MoviesService,
    private modalController: ModalController,
    private dataLocalService: DataLocalService
  ) { }

  ngOnInit() {
    this.dataLocalService.existePelicula(this.id).then( existe => this.estrella = (existe) ? 'star' : 'star-outline');
    this.getPeliculaDetalle();
    this.getActoresPelicula();
  }

  getPeliculaDetalle(){
    this.moviesService.getPeliculaDetalle(this.id).subscribe( (pelicula) => {
      this.pelicula = pelicula;
    });
  }

  getActoresPelicula(){
    this.moviesService.getActoresPelicula(this.id).subscribe( ({cast}) => {
      this.actores = cast;
    });
  }

  regresar(){
    this.modalController.dismiss();
  }

  favorito(){
    const existe = this.dataLocalService.guardarPelicula(this.pelicula);
    this.estrella = (existe) ? 'star' : 'star-outline';
  }
}
