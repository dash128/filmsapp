import { Component, OnInit } from '@angular/core';
import { IPeliculaDetalle, IGenre } from '../interfaces/interfaces';
import { ModalController } from '@ionic/angular';
import { DataLocalService } from '../services/data-local.service';
import { MoviesService } from '../services/movies.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit{
  favoritos: IPeliculaDetalle[] = [];
  generos: IGenre[] = [];
  peliculasPorGenero: {[key: string]: IPeliculaDetalle[]} = {};

  constructor(
    private dataLocalService: DataLocalService,
    private moviesService: MoviesService,
    private modalController: ModalController
  ) {}

  async ngOnInit() {
    // await this.cargarFavoritos();
    // await this.getGeneros();
    // await this.crearPeliculasPorGenero();

  }

  async ionViewWillEnter(){
    await this.cargarFavoritos();
    await this.getGeneros();
    await this.crearPeliculasPorGenero();
  }

  async cargarFavoritos(){
    this.favoritos = await this.dataLocalService.cargarFavoritos();;
  }

  async getGeneros(){
    this.generos = await this.moviesService.getListarGeneros( );
  }

  async crearPeliculasPorGenero(){
    this.peliculasPorGenero = {};

    for(const favorito of this.favoritos){
      favorito.genres.map( genero => {
        if(genero.name in this.peliculasPorGenero){
          this.peliculasPorGenero[genero.name].push(favorito);
        }else{
          if(this.peliculasPorGenero[genero.name]){
            this.peliculasPorGenero[genero.name].push(favorito);
          }else{
            this.peliculasPorGenero[genero.name] = [];//.push(favorito);
            this.peliculasPorGenero[genero.name].push(favorito);
          }

        }
      });
    }
  }

  async recargarFavoritos(){
    await this.cargarFavoritos();
    await this.crearPeliculasPorGenero();
  }
}
