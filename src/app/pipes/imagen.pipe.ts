/* eslint-disable curly */
import { Pipe, PipeTransform } from '@angular/core';
import { environment } from '../../environments/environment';

const URL = environment.imgPath;

@Pipe({
  name: 'imagen'
})
export class ImagenPipe implements PipeTransform {

  transform(img: string, size: string = 'w500'): string {
    if(!img) return './assets/img_not_found.png';

    const imgUrl = `${URL}/${size}/${img}`;
    return imgUrl;
  }

}
