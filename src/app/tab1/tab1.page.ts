import { Component, OnInit } from '@angular/core';
import { MoviesService } from '../services/movies.service';
import { IPelicula } from '../interfaces/interfaces';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit {
  peliculasRecientes: IPelicula[] = [];
  populares: IPelicula[] = [];


  constructor(
    private moviesService: MoviesService
  ) {}

  ngOnInit(): void {
    this.getMovies();
    this.getPopulares();
  }

  getMovies(){
    this.moviesService.getFeatures().subscribe( ({results}) => {
      this.peliculasRecientes = results;
    });
  }

  getPopulares(){
    this.moviesService.getPopulares().subscribe( ({results}) => {
      // this.populares.push(...results);
      const arrTemp = [...this.populares, ...results];
      this.populares = arrTemp;
    });
  }

  cargarMasPeliculas(){
    this.getPopulares();
  }
}
