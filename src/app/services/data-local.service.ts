/* eslint-disable no-underscore-dangle */
import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { IPeliculaDetalle } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class DataLocalService {
  peliculas: IPeliculaDetalle[] = [];
  private _storage: Storage | null = null;

  constructor(
    private storage: Storage,
    private toastController: ToastController
  ) {
    this.init();
  }

  async init() {
    const storage = await this.storage.create();
    this._storage = storage;
    await this.cargarFavoritos();
  }

  guardarPelicula(pelicula: IPeliculaDetalle){
    const existe = this.peliculas.find( peli => peli.id === pelicula.id);

    if(!existe){
        this.peliculas.push(pelicula);
        this.mostrarToast('Película agregada');
    }else{
      this.peliculas = this.peliculas.filter( peli => peli.id !== pelicula.id);
      this.mostrarToast('Película removida');
    }

    this._storage.set('peliculas', this.peliculas);

    return !existe;
  }

  async cargarFavoritos(){
    // let storageData = await this
    const favoritos = await this.storage.get('peliculas');
    this.peliculas = favoritos || [];

    return this.peliculas;
  }

  async existePelicula(id: string){
    this.cargarFavoritos();
    const existe = this.peliculas.find( peli => peli.id === Number(id));

    return (existe) ? true : false;
  }

  async mostrarToast(message: string){
    const toast = await this.toastController.create({
      message,
      duration: 1500
    });

    toast.present();
  }
}
