/* eslint-disable @typescript-eslint/member-ordering */
/* eslint-disable max-len */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IRespuestaMDB, IPelicula, IPeliculaDetalle, IRespuestaCredito, IRespuestaBusqueda, IRespuestaGeneros, IGenre } from '../interfaces/interfaces';
import { environment } from '../../environments/environment';

const URL = environment.url;
const APIKEY = environment.apiKey;

@Injectable({
  providedIn: 'root'
})
export class MoviesService {
  private popularesPagina = 0;
  private generos: IGenre[] = [];

  constructor(
    private http: HttpClient
  ) { }

  private ejecutarQuery<T>(query: string) {
    query = URL + query;
    query += `&api_key=${APIKEY}&language=es&include_image_language=es`;

    return this.http.get<T>(query);
  }

  getFeatures(){
    const hoy = new Date();
    const ultimoDia = new Date(hoy.getFullYear(), hoy.getMonth()+1, 0).getDate();
    const mes = hoy.getMonth() + 1;
    let mesString;

    if(mes<10){
      mesString = '0' + mes;
    }else{
      mesString = mes;
    }

    const inicio = `${hoy.getFullYear()}-${mesString}-01`;
    const fin = `${hoy.getFullYear()}-${mesString}-${ultimoDia}`;

    return this.ejecutarQuery<IRespuestaMDB>(`/discover/movie?primary_release_date.gte=${inicio}&primary_release_date.lte=${fin}`);
  }

  getPopulares(){
    this.popularesPagina++;
    const query = `/discover/movie?sort_by=popularity.desc&page=${this.popularesPagina}`;
    return this.ejecutarQuery<IRespuestaMDB>(query);
  }

  getPeliculaDetalle(id: string){
    const query = `/movie/${id}?a=1`;
    return this.ejecutarQuery<IPeliculaDetalle>(query);
  }

  getActoresPelicula(id: string){
    const query = `/movie/${id}/credits?a=1`;
    return this.ejecutarQuery<IRespuestaCredito>(query);
  }

  getBuscarPeliculas(text: string){
    const query = `/search/movie?query=${text}`;
    return this.ejecutarQuery<IRespuestaBusqueda>(query);
  }

  getListarGeneros(): Promise<IGenre[]>{
    return new Promise( resolve => {
      const query = `/genre/movie/list?a=1`;
      return this.ejecutarQuery<IRespuestaGeneros>(query).subscribe( ({genres}) => {
        this.generos = genres;
        resolve(this.generos);
      });
    });

  }
}
