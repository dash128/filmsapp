import { Component } from '@angular/core';
import { MoviesService } from '../services/movies.service';
import { IPelicula } from '../interfaces/interfaces';
import { ModalController } from '@ionic/angular';
import { DetalleComponent } from '../components/detalle/detalle.component';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  textoBuscar = '';
  buscando = false;
  ideas: string[] = ['Spiderman', 'Naruto'];
  peliculas: IPelicula[] = [];

  constructor(
    private moviesService: MoviesService,
    private modalController: ModalController
  ) {}

  buscar({detail}){
    if(detail.value.length === 0){
      this.buscando = false;
      this.peliculas = [];
      return;
    }
    this.buscando = true;
    this.buscarPeliculas(detail.value);
  }

  buscarPeliculas(text: string){
    this.moviesService.getBuscarPeliculas(text).subscribe( ({results})=>{
      this.peliculas = results;
      this.buscando = false;
    });
  }

  async verDetalle(id: string){
    const modal = await this.modalController.create({
      component: DetalleComponent,
      componentProps: {
        id
      }
    });

    modal.present();
  }
}
